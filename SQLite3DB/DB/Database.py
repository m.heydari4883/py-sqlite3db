import sqlite3 as sqlite

from django.http import HttpResponse


class Database:

    def __init__(self, db_name):
        self._db_name = db_name

        self._db_connection = None
        self._db_cursor = None

    def __enter__(self):
        self.open_db(self._db_name)

    def open_db(self, db_name):
        try:
            self._db_connection = sqlite.connect(db_name)
            self._db_cursor = self._db_connection.cursor()
        except sqlite.Error as error:
            print(f'failed to make a connection to sqlite database with {db_name} db_name, Error -> {error}')

    def close_db(self):
        if self._db_connection:
            self._db_connection.commit()
            self._db_cursor.close()
            self._db_connection.close()

    def create_table(self, table_name, columns):
        create_table_sql = f'CREATE TABLE IF NOT EXISTS {table_name} ({columns})'
        self._db_cursor.execute(create_table_sql)

    def get_all(self, table_name):
        query = f'SELECT * from {table_name}'
        self._db_cursor.execute(query)

        # fetch data
        rows = self._db_cursor.fetchall()

        return rows

    def insert(self, table_name, columns=None, data=None):
        insert_query = f"INSERT INTO {table_name} ({columns}) VALUES ({data})"

        self._db_cursor.execute(insert_query)
        self._db_connection.commit()

    def query(self, sql):
        self._db_cursor.execute(sql)

    def delete(self, table_name, where_clause):
        delete_sql = f'DELETE FROM {table_name} WHERE {where_clause}'
        self._db_cursor.execute(delete_sql)
        self._db_connection.commit()

    def update(self, table_name, set_clause, where_clause):
        update_sql = f'UPDATE {table_name} SET {set_clause} WHERE {where_clause}'
        self._db_cursor.execute(update_sql)
        self._db_connection.commit()
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close_db()
